/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amancio.rhea.java.pkg2.pkg02232021;
import java.util.Random;
/**
 *
 * @author 2ndyrGroupA
 */
public class AmancioRheaJava202232021 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //DECK SHUFFLE
        System.out.println("\nDeck Shuffle:\n");
        Deck deck = new Deck();
        deck.shuffle();
        for (int suit=1; suit<=Deck.numSuits; suit++){
            for (int rank=1; rank<=Deck.numRanks; rank++){
                Card card = deck.getCard(suit, rank);
                //System.out.println(card.rank + " "+ card.suit);
                System.out.println(card.rankToName(card.rank)+" "+card.suitToName(card.suit));
            }
        }
        
        //GRADEBOOK
        System.out.println("\nGrade Book:\n");
        int gradesArray[][] = { 
            { 87, 96, 70 }, 
            { 68, 87, 90 }, 
            { 94, 100, 90 }, 
            { 100, 81, 82 }, 
            { 83, 65, 85 }, 
            { 78, 87, 65 }, 
            { 85, 75, 83 }, 
            { 91, 94, 100 }, 
            { 76, 72, 84 }, 
            { 87, 93, 73 } }; 
        GradeBook myGradeBook = new GradeBook("Java Programming 2", gradesArray );
        myGradeBook.displayMessage();
        myGradeBook.processGrades();
    }
}