/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amancio.rhea.java.pkg2.pkg02232021;

/**
 *
 * @author 2ndyrGroupA
 */
public class DeckDisplay {
    public static void main(String[] args){
        Deck deck = new Deck();
        deck.shuffle();
        for (int suit=1; suit<=Deck.numSuits; suit++){
            for (int rank=1; rank<=Deck.numRanks; rank++){
                Card card = deck.getCard(suit, rank);
                //System.out.println(card.rank + " "+ card.suit);
                System.out.println(card.rankToName(card.rank)+" "+card.suitToName(card.suit));
            }
        }
    }
}
